# Flectra Community / geospatial

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_google_map](base_google_map/) | 2.0.1.0.1| View modes and widgets to integrate Google Maps in your UI


